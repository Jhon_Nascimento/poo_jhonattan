package br.ufc.quixada.ce.exec;
import br.ufc.quixada.ce.model.*;

public class Principal {

	public static void main(String[] args) {
		System.out.println("****Programa de Teste****\n");
		Pessoa a1 = new Pessoa("Jhonattan", "Rua S�o Jo�o, 168.", "(85) 4002-8922");
		System.out.println("****Teste Pessoa****\n");
		System.out.println("Nome: "+a1.getNome()+"\n"+
		"Endere�o: "+a1.getEndereco()+"\n"+
				"Telefone: "+a1.getTelefone()+"\n");
		System.out.println("*******************");
		
		
		System.out.println("****Teste Fornecedor****");
		Fornecedor a2 = new Fornecedor("Francisco", "Rua Carlos Alberto de Nobrega, 86", "(11)12121232", 1800.00, 755.00);
		System.out.println("Nome: "+a2.getNome()+"\n"+
		"Endere�o: "+a2.getEndereco()+"\n"+
				"Telefone: "+a2.getTelefone()+"\n"+
			"Valor Credito: "+a2.getValorCredito()+"\n"+
			"Valor Divida: "+a2.getValorDivida()+"\n"+
			"Valor Saldo: "+a2.ObterSaldo()+"\n");
		System.out.println("*******************");
		
		System.out.println("****Teste Empregado****");
		Empregado a3 = new Empregado("Roberto", "Rua Esqueda, 23", "(22) 8290-3123", 7, 974.00, 38.0);
		System.out.println("Nome: "+a3.getNome()+"\n"+
		"Endere�o: "+a3.getEndereco()+"\n"+
				"Telefone: "+a3.getTelefone()+"\n"+
			"Codigo Setor: "+a3.getCodigoSetor()+"\n"+
			"Salario Base: "+a3.getSalarioBase()+"\n"+
			"Imposto: "+a3.getImposto()+"\n"+
			"Salario Final: "+a3.CalcularSalario()+"\n"
			);
		System.out.println("*******************");
		
		
		System.out.println("****Teste Administrador****");
		Administrador a4 = new Administrador("Renatinho", "Av. do Mercado, 1200", "(99) 1209-8876", 3, 974.00, 50.0, 230.0);
		System.out.println("Nome: "+a4.getNome()+"\n"+
		"Endere�o: "+a4.getEndereco()+"\n"+
				"Telefone: "+a4.getTelefone()+"\n"+
			"Codigo Setor: "+a4.getCodigoSetor()+"\n"+
			"Salario Base: "+a4.getSalarioBase()+"\n"+
			"Imposto: "+a4.getImposto()+"\n"+
			"Ajuda de Custo: "+a4.getAjudaDeCusto()+"\n"+
			"Salario Final: "+a4.SalarioAdministrador()+" \n"
			);
		System.out.println("*******************");

		System.out.println("****Teste Oper�rio****");
		Operario a5 = new Operario("Carla", "Av. Basilio Pinto, 766", "(15) 9405-3321", 1, 974.00, 30.0, 50.0, 12.5);
		System.out.println("Nome: "+a5.getNome()+"\n"+
		"Endere�o: "+a5.getEndereco()+"\n"+
				"Telefone: "+a5.getTelefone()+"\n"+
			"Codigo Setor: "+a5.getCodigoSetor()+"\n"+
			"Salario Base: "+a5.getSalarioBase()+"\n"+
			"Imposto: "+a5.getImposto()+"\n"+
			"Valor de Producao: "+a5.getValorProducao()+"\n"+
			"Comiss�o: "+a5.getComiss�o()+"\n"+
			"Salario Final: "+a5.CalculaSalarioOperario()+"\n"
			);
		System.out.println("*******************");
		
		System.out.println("****Teste Vendedor****");

		Vendedor a6 = new Vendedor("Fernanda", "Rua Alberto S�, 545", "(55) 9921-4405", 2, 974.00, 60.0, 75.0, 20.0);
		System.out.println("Nome: "+a6.getNome()+"\n"+
		"Endere�o: "+a6.getEndereco()+"\n"+
				"Telefone: "+a6.getTelefone()+"\n"+
			"Codigo Setor: "+a6.getCodigoSetor()+"\n"+
			"Salario Base: "+a6.getSalarioBase()+"\n"+
			"Imposto: "+a6.getImposto()+"\n"+
			"Valor de Vendas: "+a6.getValorVendas()+"\n"+
			"Comiss�o: "+a6.getComissao()+"\n"+
			"Salario Final: "+a6.CalcularSalarioVendedor()+"\n"
			);
		System.out.println("*******************");
		
	}

}
