package br.ufc.quixada.ce.model;

public class Operario extends Empregado {

	private double valorProducao;
	private double comiss�o;
	
	public Operario() {
		
	}
	
	public Operario(String nome, String endereco, String telefone, int codigoSetor, double salarioBase, double imposto,double valorProducao, double comiss�o) {
		super(nome,  endereco, telefone, codigoSetor, salarioBase, imposto);
		this.valorProducao = valorProducao;
		this.comiss�o = comiss�o;
	}
	
	public double getValorProducao() {
		return valorProducao;
	}
	public void setValorProducao(double valorProducao) {
		this.valorProducao = valorProducao;
	}
	public double getComiss�o() {
		return comiss�o;
	}
	public void setComiss�o(double comiss�o) {
		this.comiss�o = comiss�o;
	}
	
	public double CalculaSalarioOperario() {
		double comiss = ((valorProducao/100) * comiss�o);
		return super.CalcularSalario() + comiss;
	}
	
}
