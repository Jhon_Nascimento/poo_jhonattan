package br.ufc.quixada.si.exec;
import java.time.LocalDate;
import java.util.Scanner;

import br.ufc.quixada.si.model.Pessoa;
import br.ufc.quixada.si.model.PessoaFisica;
import br.ufc.quixada.si.model.PessoaJuridica;
import br.ufc.quixada.si.model.Agenda;

public class Principal {
	public static Scanner teclado = new Scanner(System.in);
	public static void main(String[] args) {
		Agenda qualquer = new Agenda();
		LocalDate a = LocalDate.of(1998, 8, 12);
		LocalDate b = LocalDate.of(1800, 2, 22);
		LocalDate c = LocalDate.of(2000, 2, 13);
		LocalDate d = LocalDate.of(1999, 6, 7);
		Pessoa a1 = new PessoaFisica("Franciscleidison", "franciscleidison@gmail.com", "Rua do Santo amaro n� 12", a,12, "Desquitado");
		Pessoa a2 = new PessoaFisica("Bruno", "docedeovo@gmail.com", "Rua do Santo amaro n� 12", b, 7, "Casado");
		Pessoa a3 = new PessoaJuridica("kely", "kelykkk@gmail.com", "Rua do Santo amaro n� 12", c, 43, 100, "Lojinha");
		Pessoa a4 = new PessoaJuridica("tierry henry", "henry@gmail.com", "Rua do Santo amaro n� 12", d, 21, 120, "lojona");

		qualquer.addCliente(a1);
		qualquer.addCliente(a2);
		qualquer.addCliente(a3);
		qualquer.addCliente(a4);
		
		System.out.println("Digite o nome: ");
		String nome = teclado.next();
		System.out.print("\n");
		System.out.println("Pesquisando\n\n");
		qualquer.pesquisarCliente(nome);
		System.out.println("Listando\n\n");
		qualquer.listarCliente();
		System.out.println("Removendo\n\n");
		qualquer.removeCliente(nome);
		System.out.println("Listando\n\n");
		qualquer.listarCliente();
		System.out.println("ordenando\n\n");
		qualquer.ordenar();
		System.out.println("listando\n\n");
		qualquer.listarCliente();
		
		
	//	qualquer.listarCliente();
	//	System.out.println("Digite o nome:");
		//String nome2 = teclado.next();
		//System.out.print("\n");
		
	
		//qualquer.removeCliente(nome);
		/*System.out.println("Digite o cpf: \n");
		int cpf = teclado.nextInt();
		System.out.println("Digite o cnpj: \n");
		int cnpj = teclado.nextInt();*/
		//qualquer.removeCliente(nome);
	}
}
