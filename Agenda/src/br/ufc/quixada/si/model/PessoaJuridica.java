package br.ufc.quixada.si.model;

import java.time.LocalDate;

public class PessoaJuridica extends Pessoa{

	private int cnpj;
	private int inscricaoEstadual;
	private String razaoSocial;
	
	public PessoaJuridica() {
		
	}
	
	
	
	public PessoaJuridica(String nome, String email, String endereco, LocalDate data_De_Nasc, int cnpj, int inscricaoEstadual, String razaoSocial) {
		super(nome, email, endereco, data_De_Nasc);
		this.cnpj = cnpj;
		this.inscricaoEstadual = inscricaoEstadual;
		this.razaoSocial = razaoSocial;
	}

	public int getCnpj() {
		return cnpj;
	}
	public void setCnpj(int cnpj) {
		this.cnpj = cnpj;
	}
	public int getInscricaoEstadual() {
		return inscricaoEstadual;
	}
	public void setInscricaoEstadual(int inscricaoEstadual) {
		this.inscricaoEstadual = inscricaoEstadual;
	}
	public String getRazaoSocial() {
		return razaoSocial;
	}
	public void setRazaoSocial(String razaoSocial) {
		this.razaoSocial = razaoSocial;
	}



	@Override
	public String toString() {
		
		return super.toString() + "cnpj=" + cnpj +"\n" +"Inscricao Estadual = " + inscricaoEstadual +  "\n" + "RazaoSocial = "
				+ razaoSocial + "\n";
	}
	
	
	
}
