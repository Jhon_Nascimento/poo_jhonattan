package br.ufc.quixada.si.model;

import java.time.LocalDate;

public class Pessoa {
	private String nome;
	private String email;
	private String endereco;
	private LocalDate data_De_Nasc;
	
	public Pessoa() {
		
	}
	
	public Pessoa(String nome, String email, String endereco, LocalDate data_De_Nasc) {
		super();
		this.nome = nome;
		this.email = email;
		this.endereco = endereco;
		this.data_De_Nasc = data_De_Nasc;
	}
	
	public String getNome() {
		return nome;
	}
	public void setNome(String nome) {
		this.nome = nome;
	}
	public String getEmail() {
		return email;
	}
	public void setEmail(String email) {
		this.email = email;
	}
	public String getEndereco() {
		return endereco;
	}
	public void setEndereco(String endereco) {
		this.endereco = endereco;
	}
	public LocalDate getData_De_Nasc() {
		return data_De_Nasc;
	}
	public void setData_De_Nasc(LocalDate data_De_Nasc) {
		this.data_De_Nasc = data_De_Nasc;
	}

	@Override
	public String toString() {
		return "Pessoa: \nNome = " + nome + "\n" + "Email = " + email + "\n" + "Endereco = " + endereco +"\n" + "Data_De_Nasc = " + data_De_Nasc
				+ "\n";
	}
	
	
	
}
