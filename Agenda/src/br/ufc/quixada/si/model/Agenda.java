package br.ufc.quixada.si.model;

import java.util.ArrayList;


public class Agenda {

	public ArrayList<Pessoa> pessoas;
	
	
	public Agenda() {
		pessoas = new ArrayList<>();
	}

	public void ordenar() {
	//	int i = 0, k = 0;
		for(int i = 0; i <pessoas.size(); i++) {
			int pequeno = i;
			for(int k = i + 1; k <pessoas.size();k++) {
				
				if(pessoas.get(k) instanceof PessoaFisica & pessoas.get(pequeno) instanceof PessoaFisica ) {
					if(((PessoaFisica) pessoas.get(k)).getCpf() < (((PessoaFisica) pessoas.get(pequeno)).getCpf())){
						pequeno = k;
					}
				}	
			}
			Pessoa aux = pessoas.get(i);
			pessoas.set(i, pessoas.get(pequeno));
			pessoas.set(pequeno, aux);
			
		}
		
		for(int i = 0; i <pessoas.size(); i++) {
			int pequeno = i;
			for(int k = i + 1; k <pessoas.size();k++) {
				
				if(pessoas.get(k) instanceof PessoaJuridica & pessoas.get(pequeno) instanceof PessoaJuridica ) {
					if(((PessoaJuridica) pessoas.get(k)).getCnpj() < (((PessoaJuridica) pessoas.get(pequeno)).getCnpj())){
						pequeno = k;
					}
				}	
			}
			Pessoa aux = pessoas.get(i);
			pessoas.set(i, pessoas.get(pequeno));
			pessoas.set(pequeno, aux);
			
		}
		System.out.println("Ordenado com sucesso.\n");
	}
	
	public void addCliente(Pessoa nome) {
		
		if(pessoas.add(nome)) {
			System.out.println("Contato cadastrado!\n");
		}else {
			System.out.println("Contato n�o adicionado");
		}
		
	}
	
	public void removeCliente(String n) {
		System.out.println(n);
		for(int i= 0; i < pessoas.size(); i++) {
			Pessoa cara = pessoas.get(i);			
			if(cara.getNome().equals(n)) {
				pessoas.remove(cara);
				System.out.println("O contato foi removido");
			break;
			}
		}
	}

	public void pesquisarCliente(String nome) {
		int a = 0;
		for(int i= 0; i < pessoas.size(); i++) {
			Pessoa contatos = pessoas.get(i);
			if(contatos.getNome().equals(nome)) {
				System.out.println(contatos.toString());
				a = 1;
			}
		}
		if(a == 0) {
			System.out.println("Contato n�o encontrado.\n");
		}
	}
	
	public void listarCliente() {
		for(int i= 0; i < pessoas.size(); i++) {
			System.out.println(pessoas.get(i).toString());
		}
	}
}
