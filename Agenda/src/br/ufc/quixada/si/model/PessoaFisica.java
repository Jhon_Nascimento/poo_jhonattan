package br.ufc.quixada.si.model;

import java.time.LocalDate;

public class PessoaFisica extends Pessoa {

	private int cpf;
	private String estadoCivil;
	
	public PessoaFisica() {
		
	}
	
	public PessoaFisica(String nome, String email, String endereco, LocalDate data_De_Nasc, int cpf,
			String estadoCivil) {
		super(nome, email, endereco, data_De_Nasc);
		this.cpf = cpf;
		this.estadoCivil = estadoCivil;
	}
	public int getCpf() {
		return cpf;
	}
	public void setCpf(int cpf) {
		this.cpf = cpf;
	}
	public String getEstadoCivil() {
		return estadoCivil;
	}
	public void setEstadoCivil(String estadoCivil) {
		this.estadoCivil = estadoCivil;
	}

	@Override
	public String toString() {
		return super.toString() + "cpf = " + cpf + "\n" + "Estado Civil = " + estadoCivil + "\n";
	}
	
	
}
