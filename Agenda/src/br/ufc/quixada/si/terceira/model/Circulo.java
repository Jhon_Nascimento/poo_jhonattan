package br.ufc.quixada.si.terceira.model;

public class Circulo extends Figuraa {

	private int raio;

	public Circulo(String cor, String filled, int raio) {
		super(cor, filled);
		this.raio = raio;
	}

	public int getRaio() {
		return raio;
	}

	public void setRaio(int raio) {
		this.raio = raio;
	}
	
	public void areaCirculo() {
		System.out.println("Area = "+ (3.14 * (this.raio ^ 2))+"\n");
	}
	
	public void perimetroCirculo() {
		System.out.println("Perimetro = "+ (2 * this.raio * 3.14)+"\n");
	}
	
	
}
