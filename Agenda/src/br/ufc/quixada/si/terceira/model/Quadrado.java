package br.ufc.quixada.si.terceira.model;

public class Quadrado extends Figuraa {

	private int lado;

	public Quadrado(String cor, String filled, int lado) {
		super(cor, filled);
		this.lado = lado;
	}

	public int getLado() {
		return lado;
	}

	public void setLado(int lado) {
		this.lado = lado;
	}
	
	public void areaQuadrado() {
		System.out.println("Area = "+ (this.lado ^ 2)+"\n");
	}
	
	public void perimetroQuadrado() {
		System.out.println("Perimetro = "+ (this.lado * 4)+"\n");
	}
	
}
