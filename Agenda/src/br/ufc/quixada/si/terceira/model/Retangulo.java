package br.ufc.quixada.si.terceira.model;

public class Retangulo extends Figuraa {


	private int base;
	private int altura;
	public Retangulo(String cor, String filled, int base, int altura) {
		super(cor, filled);
		this.base = base;
		this.altura = altura;
	}
	public int getBase() {
		return base;
	}
	public void setBase(int base) {
		this.base = base;
	}
	public int getAltura() {
		return altura;
	}
	public void setAltura(int altura) {
		this.altura = altura;
	}
	
	public void areaQuadrado() {
		System.out.println("Area = "+ (this.base * this.altura)+"\n");
	}
	
	public void perimetroQuadrado() {
		System.out.println("Perimetro = "+ ((this.base ^ 2) + (this.altura ^ 2))+"\n");
	}
	
}
