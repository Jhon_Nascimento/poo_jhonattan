package br.ufc.quixada.si.terceira.model;

public class Figuraa {

	private String cor;
	private String filled;
	public Figuraa(String cor, String filled) {
		super();
		this.cor = cor;
		this.filled = filled;
	}
	public String getCor() {
		return cor;
	}
	public void setCor(String cor) {
		this.cor = cor;
	}
	public String getFilled() {
		return filled;
	}
	public void setFilled(String filled) {
		this.filled = filled;
	}
	
	
}
