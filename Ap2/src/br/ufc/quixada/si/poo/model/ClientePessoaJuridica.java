package br.ufc.quixada.si.poo.model;

import java.time.LocalDate;

public class ClientePessoaJuridica extends Cliente{
	
	private String cnpj;
	private LocalDate dataAbertura;
	
	public ClientePessoaJuridica(){
		super();
	}

	public String getCnpj() {
		return cnpj;
	}

	public LocalDate getDataAbertura() {
		return dataAbertura;
	}

	public void setCnpj(String cnpj) {
		this.cnpj = cnpj;
	}

	public void setDataAbertura(LocalDate dataAbertura) {
		this.dataAbertura = dataAbertura;
	}

	@Override
	public String toString() {
		return super.toString() + "Cnpj: " + this.cnpj + "\n" + "Data de Abertura: " + this.dataAbertura + "\n";
	}
	
	

}
