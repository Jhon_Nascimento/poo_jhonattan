package br.ufc.quixada.si.poo.model;

import java.time.LocalDate;

public class Contrato {
	
	private int codContrato;
	private Cliente cliente;
	private Vendedor vendedor;
	private LocalDate dataInicio;
	private float valorContrato;
	
	
	public Cliente getCliente() {
		return cliente;
	}
	
	public void setCliente(Cliente cliente) {
		this.cliente = cliente;
	}
	
	public Vendedor getVendedor() {
		return vendedor;
	}
	
	public void setVendedor(Vendedor vendedor) {
		this.vendedor = vendedor;
	}
	
	public LocalDate getDataInicio() {
		return dataInicio;
	}
	public void setDataInicio(LocalDate dataInicio) {
		this.dataInicio = dataInicio;
	}
	
	public int getCodContrato() {
		return codContrato;
	}
	public void setCodContrato(int codContrato) {
		this.codContrato = codContrato;
	}
	public float getValorContrato() {
		return valorContrato;
	}
	public void setValorContrato(float valorContrato) {
		this.valorContrato = valorContrato;
	}

	@Override
	public String toString() {
		return "Codigo Contrato: " + this.codContrato + "\n" + "Cliente: " + this.cliente + "\n" + "Vendedor: " + this.vendedor
				+ "\n" +"Data Inicio: " + this.dataInicio+ "\n" + "ValorContrato: " + this.valorContrato + "\n";
	}
	
	
	
	
	
	

}
