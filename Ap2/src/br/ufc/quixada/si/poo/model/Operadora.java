package br.ufc.quixada.si.poo.model;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;
import java.util.Map.Entry;
import br.ufc.quixada.si.poo.interfaces.Imprimivel;

public class Operadora implements Imprimivel {
	
	private int codOperadora;
	private String nome;
	Map<Integer, Contrato> contratos;
	ArrayList<Cliente> clientes;
	
	public Operadora() {
		contratos = new HashMap<>();
		clientes = new ArrayList<>();
	}	
	
	public int getCodOperadora() {
		return codOperadora;
	}
	
	public void setCodOperadora(int codOperadora) {
		this.codOperadora = codOperadora;
	}

	public String getNome() {
		return nome;
	}
	
	public void setNome(String nome) {
		this.nome = nome;
	}
	
	public void cadastrarContratoCliente(int chave, Contrato c) {
		clientes.add(c.getCliente());
		contratos.put(chave, c);
		System.out.println("Cliente e Contrato adicionado!");
	}
	
	@Override
	public String toString() {
		return "Nome: " + this.nome + "\n";
	}

	@Override
	public void mostrarContatos() {
		for(Entry<Integer, Contrato> print: contratos.entrySet()) {
			System.out.println("Codigo Operadora: " + print.getKey() + "\n"  + print.getValue().toString());
		}
	}

}
