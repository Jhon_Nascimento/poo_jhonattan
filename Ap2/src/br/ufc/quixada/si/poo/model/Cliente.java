package br.ufc.quixada.si.poo.model;

public abstract class Cliente extends Pessoa{
	private String endereco;
	
	
	public Cliente() {
	
	}

	public String getEndereco() {
		return endereco;
	}

	public void setEndereco(String endereco) {
		this.endereco = endereco;
	}

	@Override
	public String toString() {
		return  super.toString() + "Endereco: " + this.endereco + "\n";
	}
	
	
}
