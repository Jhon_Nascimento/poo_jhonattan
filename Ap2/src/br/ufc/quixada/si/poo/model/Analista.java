package br.ufc.quixada.si.poo.model;

public class Analista extends Funcionario{
	
	public Analista() {
		
	}

	@Override
	public void darBonificacao() {
		super.setSalario((float) (super.getSalario() + (super.getSalario() * 0.02)));
	}
	
	public void processarContrato(Operadora op, Contrato c) {
		op.cadastrarContratoCliente(c.getCodContrato(), c);
		this.darBonificacao();
		
	}

	@Override
	public String toString() {
		return super.toString();
	}
	
	
	

}
