package br.ufc.quixada.si.poo.model;

public class Vendedor extends Funcionario{

	public Vendedor() {
		super();
	}
	
	@Override
	public void darBonificacao() {
		super.setSalario(super.getSalario() + super.getSalario() / 20);
	}
	
	public void realizarvendas(float valorContrato) {
		super.setSalario((float) (super.getSalario() + (valorContrato * 0.5)));
		this.darBonificacao();
	}

	@Override
	public String toString() {
		return super.toString();
	}
	
	
	

}
