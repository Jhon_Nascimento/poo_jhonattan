package br.ufc.quixada.si.poo.model;

public abstract class Funcionario extends Pessoa{
	
	private String cpf;
	private String matricula;
	private float salario;
	
	public Funcionario() {
		super();
	}

	public String getCpf() {
		return cpf;
	}

	public String getMatricula() {
		return matricula;
	}

	public float getSalario() {
		return salario;
	}

	public void setCpf(String cpf) {
		this.cpf = cpf;
	}

	public void setMatricula(String matricula) {
		this.matricula = matricula;
	}

	public void setSalario(float salario) {
		this.salario = salario;
	}
	
	public abstract void darBonificacao();

	@Override
	public String toString() {
		return super.toString() +  "Cpf: " + this.cpf + "\n" + "Matricula: " + this.matricula + "\n" + "Salario: " + this.salario + "\n";
	}
	
	

}
