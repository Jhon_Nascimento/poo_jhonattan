package br.ufc.quixada.si.poo.model;

import java.time.LocalDate;

public class ClientePessoaFisica extends Cliente {
	private String cpf;
	private LocalDate dataNascimento;
	
	public ClientePessoaFisica() {
		super();
	}

	public String getCpf() {
		return cpf;
	}

	public LocalDate getDataNascimento() {
		return dataNascimento;
	}

	public void setCpf(String cpf) {
		this.cpf = cpf;
	}

	public void setDataNascimento(LocalDate dataNascimento) {
		this.dataNascimento = dataNascimento;
	}

	@Override
	public String toString() {
		return super.toString() + "Cpf: " + this.cpf + "\n" + "Data de Nascimento: " + this.dataNascimento + "\n";
	}
	
	
	

}
