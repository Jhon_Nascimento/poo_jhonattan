package br.ufc.quixada.si.poo.model;

public class ServicosGerais extends Funcionario{
	
	public ServicosGerais() {
		super();
	}

	@Override
	public void darBonificacao() {
		super.setSalario(super.getSalario() + 3);
	}

	public void limpar() {
		this.darBonificacao();
	}
	
	@Override
	public String toString() {
		return super.toString();
	}
	
}
