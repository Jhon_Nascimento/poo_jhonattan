package br.ufc.quixada.si.poo.exec;

import java.time.LocalDate;

import br.ufc.quixada.si.poo.model.*;

public class PrincipalImprimivel {

	public static void main(String[] args) {
		Cliente c1 = new ClientePessoaFisica();
		c1.setNome("Joao Cleber");
		c1.setEndereco("Rua Basilio Emiliano Pinto, 1228.");
		
		Cliente c2 = new ClientePessoaFisica();
		c2.setNome("Marina Silva");
		c2.setEndereco("Rua dos Tangamandapios, 18.");
		Cliente c3 = new ClientePessoaJuridica();
		c3.setNome("Jefferson Silva");
		c3.setEndereco("Rua A, 1000.");
		
		
		Vendedor v1 = new Vendedor();
		v1.setNome("Francisgleidison");
		v1.setCpf("12345678910");
		v1.setMatricula("12309");
		v1.setSalario(800);
		
		Vendedor v2 = new Vendedor();
		v2.setNome("Gleidison");
		v2.setCpf("12310098765");
		v2.setMatricula("32309");
		v2.setSalario(600);
		Vendedor v3 = new Vendedor();
		v3.setNome("Claudinha");
		v3.setCpf("09839128309");
		v3.setMatricula("32139");
		v3.setSalario(1000);
		
		
		Contrato contra1 = new Contrato();
		contra1.setCliente(c1);
		contra1.setVendedor(v1);
		contra1.setCodContrato(001);
		contra1.setDataInicio(LocalDate.of(2000, 9, 8));
		contra1.setValorContrato(10000);
		
		Contrato contra2 = new Contrato();
		contra2.setCliente(c2);
		contra2.setVendedor(v2);
		contra2.setCodContrato(002);
		contra2.setDataInicio(LocalDate.of(2012, 2, 5));
		contra2.setValorContrato(6000);
		
		
		Contrato contra3 = new Contrato();
		contra3.setCliente(c3);
		contra3.setVendedor(v3);
		contra3.setCodContrato(003);
		contra3.setDataInicio(LocalDate.of(2008, 4, 21));
		contra3.setValorContrato(8000);
		
		Operadora op = new Operadora();
		op.setCodOperadora(10);
		op.setNome("Vendas");
		op.cadastrarContratoCliente(contra1.getCodContrato(), contra1);
		op.cadastrarContratoCliente(contra2.getCodContrato(), contra2);
		op.cadastrarContratoCliente(contra3.getCodContrato(), contra3);
		op.mostrarContatos();
	}

}
