package br.ufc.quixada.si.poo.exec;

import java.time.LocalDate;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Scanner;

import br.ufc.quixada.si.poo.model.*;

public class Principal {
	static Scanner ler = new Scanner(System.in);
	public static void main(String[] args) {
		boolean parada = true;
		ArrayList<Pessoa> pessoas = new ArrayList<Pessoa>();
		HashMap<Integer, Operadora> operadoras = new HashMap<>();
		
		do {
			MenuPrincipal();
			switch(ler.nextInt()) {
				case 1:
					Operadora op = new Operadora();
					System.out.println("Insira o codigo da Operadora: ");
					op.setCodOperadora(ler.nextInt());
					System.out.println("Insira o nome da Operadora: ");
					op.setNome(ler.next());
					operadoras.put(op.getCodOperadora(), op);
					break;
				case 2:
					SubMenu();
					switch(ler.nextInt()) {
						case 1:
							ClientePessoaFisica c1 = new ClientePessoaFisica();
							System.out.println("Inserir Nome do Cara: ");
							c1.setNome(ler.next());
							System.out.println("Inserir Endereco do Cara: ");
							c1.setEndereco(ler.next());
							System.out.println("Inserir CPF do Cara: ");
							c1.setCpf(ler.next());
							c1.setDataNascimento(LocalDate.of(2010, 2, 13));
							pessoas.add(c1);
							break;
						case 2:
							ClientePessoaJuridica c2 = new ClientePessoaJuridica();
							System.out.println("Inserir Nome do Cara: ");
							c2.setNome(ler.next());
							System.out.println("Inserir Endereco do Cara: ");
							c2.setEndereco(ler.next());
							System.out.println("Inserir CPF do Cara: ");
							c2.setCnpj(ler.next());
							c2.setDataAbertura(LocalDate.of(1999, 9, 9));
							pessoas.add(c2);
							break;
						default:
							System.out.println("Nenhuma Op�ao selecionada");
							break;
					}
					
					break;
				case 3:
					break;
				case 4:
					break;
				case 5:
					parada = false;
					break;
				default:
					System.out.println("Op��o Inexistente.");
					break;
			}
		}while(parada);
		System.out.println("Finalizado.");

	}
	public static void MenuPrincipal() {
		System.out.println("                   +------------------------------+");
		System.out.println("                   | 1-Cadastrar Operadora:       |");
		System.out.println("                   | 2-Cadastrar Cliente:         |");
		System.out.println("                   | 3-Cadastrar Funcionario:     |");
		System.out.println("                   | 4-Cadastrar um Contato:      |");
		System.out.println("                   | 5-Finalizar:                 |");
		System.out.println("                   +------------------------------+");
		System.out.println("                   Insira a sua op��o:             ");
	}
	public static void SubMenu() {
		System.out.println("                   +------------------------------+");
		System.out.println("                   | 1-Pessoa Fisica:             |");
		System.out.println("                   | 2-Pessoa Juridica:           |");
		System.out.println("                   +------------------------------+");
		System.out.println("                   Insira a sua op��o:             ");
	}

}
