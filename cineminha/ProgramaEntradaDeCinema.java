package br.ufc.quixada.si.cineminha;

public class ProgramaEntradaDeCinema {

	public static void main(String[] args) {
		EntradaDeCinema filmeA = new EntradaDeCinema("As Tran�as do Rei Careca", "12h04", 1, 1, (float) 12.50);
		EntradaDeCinema filmeB = new EntradaDeCinema("As Tran�as do Rei Careca", "12h04", 1, 2, (float) 12.50);
		EntradaDeCinema filmeC = new EntradaDeCinema("As Tran�as do Rei Careca", "12h04", 1, 3, (float) 12.50);

		System.out.println(filmeA);
		System.out.println("______________________________________");
		System.out.println(filmeB);
		System.out.println("______________________________________");
		System.out.println(filmeC);
		System.out.println("______________________________________");
		
		float desconto = filmeA.calculaValorComDesconto(12, 12, 2008);
		System.out.println("Valor com desconto: R$"+desconto+"\n");
		float desconto_1 = filmeA.calculaValorComDesconto(12, 12, 2004, 414176);
		System.out.println("Valor com desconto: R$"+desconto_1+"\n");
		float desconto_2 = filmeB.calculaValorComDesconto(21, 12, 2012);
		System.out.println("Valor com desconto: R$"+desconto_2+"\n");
		float desconto_3 = filmeB.calculaValorComDesconto(06, 02, 2000, 424176);
		System.out.println("Valor com desconto: R$"+desconto_3+"\n");
		float desconto_4= filmeC.calculaValorComDesconto(06, 06, 2006);
		System.out.println("Valor com desconto: R$"+desconto_4+"\n");
		float desconto_5 = filmeC.calculaValorComDesconto(25, 07, 1995, 434176);
		System.out.println("Valor com desconto: R$"+desconto_5+"\n");
		
		filmeA.realizaVenda();
		filmeB.realizaVenda();
		filmeC.realizaVenda();
		
		System.out.println(filmeA);
		System.out.println("______________________________________");
		System.out.println(filmeB);
		System.out.println("______________________________________");
		System.out.println(filmeC);
		System.out.println("______________________________________");
	}

}
