package br.ufc.quixada.si.cineminha;

public class EntradaDeCinema {
	private String tituloDoFilme;
	private String horario;
	private int sala;
	private int poltrona;
	private float valorOriginal;
	private boolean condicao = true;
	
	public EntradaDeCinema(String a, String b, int c, int d, float e){
		this.tituloDoFilme = a;
		this.horario = b;
		this.sala = c;
		this.poltrona = d;
		this.valorOriginal = e;
	}
	

	public String getTituloDoFilme() {
		return this.tituloDoFilme;
	}
	public String getHorario() {
		return this.horario;
	}
	public int getSala() {
		return this.sala;
	}
	public int getPoltrona() {
		return this.poltrona;
	}
	public float getValorOriginal() {
		return this.valorOriginal;
	}
	public boolean getCondicao() {
		return this.condicao;
	}
	
	public float calculaValorComDesconto(int dia, int mes, int ano){
		ano = 2018 - ano;
		
		if(ano < 12) {
			valorOriginal = valorOriginal / 2;
		}
		
		
		return valorOriginal;
	}
	public float calculaValorComDesconto(int dia, int mes, int ano, int carteira){
		ano = 2018 - ano;
		
		if((ano >= 12) && (ano <= 15)) {
			valorOriginal -= (2 * (valorOriginal / 5));
		}else
			if((ano >=16)&&(ano <= 20)) {
				valorOriginal -= ((3 *(valorOriginal / 5)) / 2);
			}else
				if(ano > 20) {
					valorOriginal -= (valorOriginal / 5);
				}	
		return valorOriginal;
	}
	
	public void realizaVenda() {
			if(condicao == true) {
				condicao = false;
		}
	}
	public String toString() {
		String padrao = "";
		padrao = "O nome do filme: "+this.tituloDoFilme+"\n"+
		"Horario da Sess�o: "+this.horario+"\n"+
				"A sala da Sess�o: "+this.sala+"\n"+
		"O Valor do ingresso: "+this.valorOriginal+"\n"+
				"A poltrona atual: "+this.poltrona+"\n"+
		"Acento est� ocupado?: "+this.condicao;
		return padrao;
	}	
}
