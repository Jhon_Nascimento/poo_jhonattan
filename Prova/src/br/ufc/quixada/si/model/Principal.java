package br.ufc.quixada.si.model;

import br.ufc.quixada.si.exec.*;

public class Principal {

	public static void main(String[] args) {
		MotoTaxi motorista1= new MotoTaxi("Jos�", "123456", "ABC-1515", 8.8F);
		MotoTaxi motorista2=new MotoTaxi("Fabinho", "456321", "RPG-2644", 5.3F);
		Cliente cliente1 = new Cliente("Pedro", "992.827.123.40", "Idoso");
		Cliente cliente2= new Cliente("Carlos", "553.234.666- 22", "Professor");
		Cliente cliente3= new Cliente("Paula", "121.233.435-11", "Estudante");
		Cliente cliente4 = new Cliente("Vera", "886.232.626.40", "Policial");
		Corrida corrida1 = new Corrida("Pinheiro", "UFC", 3.0F);
		Corrida corrida2 = new Corrida("Planalto", "UFC", 2.5F);
		Corrida corrida3 = new Corrida("Centro", "UECE", 2.0F);
		Corrida corrida4 = new Corrida("Construtec","ALto do S�o Jo�o", 4.0F);
		
/*System.out.println(cliente1.solicitarDesconto(12.5F));
		System.out.println(cliente2.solicitarDesconto(7.5F));
		System.out.println(cliente3.solicitarDesconto(5.0F));
		System.out.println(cliente4.solicitarDesconto(17.0F));*/
		cliente1.solicitarDesconto(12.5F);
		cliente2.solicitarDesconto(7.5F);
		cliente3.solicitarDesconto(5.0F);
		cliente4.solicitarDesconto(17.0F);

		System.out.println("***********Corridas com Motorista 1***********");
		motorista1.realizarCorrida(cliente1, corrida1);
		System.out.println("________________________________________");
		motorista1.realizarCorrida(cliente2, corrida2);
		System.out.println("________________________________________");
		motorista1.realizarCorrida(cliente3, corrida3);
		System.out.println("________________________________________");
		motorista1.realizarCorrida(cliente4, corrida4);
		System.out.println("________________________________________");
		
		System.out.println("***********Corridas com Motorista 2***********");
		motorista2.realizarCorrida(cliente1, corrida1);
		System.out.println("________________________________________");
		motorista2.realizarCorrida(cliente2, corrida2);
		System.out.println("________________________________________");
		motorista2.realizarCorrida(cliente3, corrida3);
		System.out.println("________________________________________");
		motorista2.realizarCorrida(cliente4, corrida4);
		System.out.println("________________________________________");
		
		
/*System.out.println(corrida1.calcularValorCorrida(7));
		System.out.println(corrida2.calcularValorCorrida(6));
		System.out.println(corrida3.calcularValorCorrida(3));
		System.out.println(corrida4.calcularValorCorrida(11));
		*/
		corrida1.calcularValorCorrida(7);
		corrida2.calcularValorCorrida(6);
		corrida3.calcularValorCorrida(3);
		corrida4.calcularValorCorrida(11);
		
		
	}

}
