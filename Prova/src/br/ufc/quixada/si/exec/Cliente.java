package br.ufc.quixada.si.exec;

public class Cliente {

	private String nome;
	private String cpf;
	private String tipo;
	
	public Cliente(String nome, String cpf, String tipo){
		this.nome = nome;
		this.cpf = cpf;
		this.tipo = tipo;
		
	}

	public String getNome() {
		return nome;
	}

	public void setNome(String nome) {
		this.nome = nome;
	}

	public String getCpf() {
		return cpf;
	}

	public void setCpf(String cpf) {
		this.cpf = cpf;
	}

	public String getTipo() {
		return tipo;
	}

	public void setTipo(String tipo) {
		this.tipo = tipo;
	}
	
	public float solicitarDesconto(float valor) {
		if((this.tipo == "Estudante") || (this.tipo == "Idoso")) {
			valor =  valor - (valor / 2);
		}else
			if(this.tipo == "Policial") {
				valor = valor - (valor / 5);
			}else
				if(this.tipo == "Professor"){
					valor = valor - (valor / 10);
				}else {
				}
				
		return valor;
	}
	public String toString() {
		String printa;
		printa = "Nome do CLiente: " + this.nome + "\n"+
		"N�mero do CPF: "+this.cpf+ "\n"+
				"Tipo de Cliente: "+this.tipo;
		return printa;
	}
}
