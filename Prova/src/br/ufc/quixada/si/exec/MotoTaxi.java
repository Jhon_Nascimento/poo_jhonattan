package br.ufc.quixada.si.exec;

public class MotoTaxi {
	private String nome;
	private String cnh;
	private String placa;
	private float nota;
	
	public MotoTaxi(String nome, String cnh, String placa, float nota) {
		this.nome = nome;
		this.cnh = cnh;
		this.placa = placa;
		this.nota = nota;
	}

	public String getNome() {
		return nome;
	}

	public void setNome(String nome) {
		this.nome = nome;
	}

	public String getCnh() {
		return cnh;
	}

	public void setCnh(String cnh) {
		this.cnh = cnh;
	}

	public String getPlaca() {
		return placa;
	}

	public void setPlaca(String placa) {
		this.placa = placa;
	}

	public float getNota() {
		return nota;
	}

	public void setNota(float nota) {
		this.nota = nota;
	}

	public void realizarCorrida(Cliente cliente, Corrida corrida) {
		System.out.println("Nome do Cliente: " +cliente.getNome()+"\n" +
	"Nome do Moto Taxi: "+this.nome+ "\n"+ 
				"Nota do Moto Taxi: " + this.nota +"\n"+
	"Local de Partida: " + corrida.getPartida()+"\n" + 
				"Destino Final: "+corrida.getDestino());
		
	}
	public String toString() {
		String dado;
		dado = "Nome do Moto Taxi: " + this.nome +"\n" +
		"N�mero da CNH: " + this.cnh + "\n"+ 
				"Placa da Moto: " + this.placa +"\n" +
		"Nota do Moto Taxi: "+ this.nota;
		return dado;
	}
}
