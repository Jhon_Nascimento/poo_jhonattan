package br.ufc.quixada.si.exec;

public class Corrida {
	private String partida;
	private String destino;
	private float precoKm;
	private float precoCorrida;
	
	public Corrida(String partida, String destino, float precoKm) {
		this.partida = partida;
		this.destino = destino;
		this.precoKm = precoKm;
	}

	public String getPartida() {
		return partida;
	}

	public void setPartida(String partida) {
		this.partida = partida;
	}

	public String getDestino() {
		return destino;
	}

	public void setDestino(String destino) {
		this.destino = destino;
	}

	public float getPrecoKm() {
		return precoKm;
	}

	public void setPrecoKm(float precoKm) {
		this.precoKm = precoKm;
	}

	public float getPrecoCorrida() {
		return precoCorrida;
	}

	public void setPrecoCorrida(float precoCorrida) {
		this.precoCorrida = precoCorrida;
	}
	public float calcularValorCorrida(int distancia) {
		this.precoCorrida = (distancia * this.precoKm) + 5;
		return this.precoCorrida;
	}


	public String toString() {
		String modelo;
		modelo = "Local de Partida: " + this.partida +"\n" +
		"Local de destino:" + this.destino + "\n"+ 
				"Pre�o por Km: " + this.precoKm +"\n" +
		"Pre�o da Corrida: "+ this.precoCorrida;
		return modelo;
	}
	
	
}
