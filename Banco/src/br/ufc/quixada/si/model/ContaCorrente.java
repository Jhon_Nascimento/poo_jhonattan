package br.ufc.quixada.si.model;

public class ContaCorrente extends ContaBancaria implements Interface{
	private int taxaDeOperacao;
	
	
	public ContaCorrente(int numeroConta, double saldo, int taxaDeOperacao) {
		super(numeroConta, saldo);
		this.taxaDeOperacao = taxaDeOperacao;
	}


	public int getTaxaDeOperacao() {
		return taxaDeOperacao;
	}


	public void setTaxaDeOperacao(int taxaDeOperacao) {
		this.taxaDeOperacao = taxaDeOperacao;
	}


	@Override
	public boolean Sacar(double saque) {
		setSaldo(getSaldo() - (saque + getTaxaDeOperacao()));
		return true;
	}


	@Override
	public boolean Depositar(double deposito) {
		setSaldo(getSaldo() + (deposito));
		return true;
	}


	@Override
	public void mostrarDados() {
		System.out.println("Tipo: Conta Corrente"+"\n"+"Numero da Conta: "+getNumeroConta()+"\n"+"Saldo: "+ getSaldo() +"\n"+ "Taxa de Opera��o: "+ getTaxaDeOperacao() +"\n");
		
	}
	
	
	
	
}
