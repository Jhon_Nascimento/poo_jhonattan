package br.ufc.quixada.si.model;

public class ContaPoupanca extends ContaBancaria implements Interface{
	private int limite;

	
	
	public ContaPoupanca(int numeroConta, double saldo, int limite) {
		super(numeroConta, saldo);
		this.limite = limite;
	}
	public int getLimite() {
		return limite;
	}

	public void setLimite(int limite) {
		this.limite = limite;
	}

	@Override
	public boolean Sacar(double saque) {
		double atual = getSaldo() - saque;
		if((saque > getSaldo()) && (atual >= -limite)) {
			setSaldo(atual);
			System.out.println("Saque realizado com sucesso");
			return true;
		}else {
			System.out.println("Saque n�o pode ser realizado");
			return false;
		}
	}

	@Override
	public boolean Depositar(double deposito) {
		setSaldo(getSaldo() + deposito);
		return false;
	}

	@Override
	public void mostrarDados() {
		System.out.println("Tipo: Conta Poupan�a"+"\n"+"Numero da Conta: "+getNumeroConta()+"\n"+"Saldo: "+ getSaldo() +"\n"+ "Limite: "+ getLimite() +"\n");
		
	}

}
