package br.ufc.quixada.si.model;

public abstract class ContaBancaria {
	
	private int numeroConta;
	private double saldo;
	
	
	public ContaBancaria(int numeroConta, double saldo) {
		super();
		this.numeroConta = numeroConta;
		this.saldo = saldo;
	}
	
	public int getNumeroConta() {
		return numeroConta;
	}
	public void setNumeroConta(int numeroConta) {
		this.numeroConta = numeroConta;
	}
	public double getSaldo() {
		return saldo;
	}
	public void setSaldo(double saldo) {
		this.saldo = saldo;
	}
	
	public boolean Transferir(double saldo, ContaBancaria recebedor) {
		setSaldo(getSaldo() - saldo);
		recebedor.Depositar(saldo);
			return true;	
	}
	
	public abstract boolean Sacar(double saldo);
	public abstract boolean Depositar(double saldo);
	

}
