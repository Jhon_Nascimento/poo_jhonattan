package br.ufc.quixada.si.model;

import java.util.ArrayList;

public class Banco implements Interface {
	
	public static ArrayList<ContaBancaria> array;
	
	
	public Banco() {
		array = new ArrayList<ContaBancaria>();
	}
	
	public void addConta(ContaBancaria conta) {
		
		if(array.add(conta)) {
			System.out.println("Conta cadastrado!\n");
		}else {
			System.out.println("Conta n�o adicionado");
		}
		
	}
	
	public void removeCliente(int numeroRemover) {
		System.out.println(numeroRemover);
		boolean teste = false;
		for(int i= 0; i < array.size(); i++) {
			ContaBancaria conta = array.get(i);		
			if(conta.getNumeroConta() == numeroRemover) {
				array.remove(conta);
				System.out.println("O contato foi removido");
				teste = true;
				break;
			}
		}
		if(teste == false) {
			System.out.println("Conta n�o existe ou  Nenhuma conta cadastrada!");
		}
	}
	
	public  ContaBancaria pesquisarCliente(int numeroRemover) {
		int a = 0;
		for(int i= 0; i < array.size(); i++) {
			ContaBancaria pesquisa = array.get(i);
			if(pesquisa.getNumeroConta() == numeroRemover) {
				
				a = 1;
				return pesquisa;
			}
		}
		if(a == 0) {
			System.out.println("Contato n�o encontrado.\n");
		}
		return null;
	}
	@Override
	public void mostrarDados() {
		for (ContaBancaria tercio : array) {
			Relatorio a1 = new Relatorio();
			a1.gerarRelatorio((Interface) tercio);
		}
		
	}
}
