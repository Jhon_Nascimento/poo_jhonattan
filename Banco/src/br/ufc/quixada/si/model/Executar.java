package br.ufc.quixada.si.model;

public class Executar {

	public static void main(String[] args) {
		
		ContaBancaria conta1 = new ContaCorrente(17, 670.00, 17);
		ContaBancaria conta2 = new ContaPoupanca(29, 150.00, 200);
		conta1.Sacar(230.00);
		conta2.Sacar(350.00);
		
		Relatorio a1 = new Relatorio();
		
		a1.gerarRelatorio((Interface) conta1);
		a1.gerarRelatorio((Interface) conta2);

	}

}
