package br.ufc.quixada.si.exec;
import java.util.Scanner;

import br.ufc.quixada.si.model.*;

public class Principal {
	
	public static Scanner teclado = new Scanner(System.in);
	public static void main(String[] args) {
		Banco itau = new Banco();
		int contaselect, transferencia;
		boolean opcao = true, criar = true, select = true;
		do {
			switch( Menu() ) {
				case 1: //Criar Conta
					do {
						switch( CriandoConta() ) {
							case 1://Conta Corrente
								itau.addConta(ContaCorrente());
								criar = false;
								break;
							case 2://Conta Poupan�a
								itau.addConta(ContaPoupanca());
								criar = false;
								break;
							default:
								System.out.println("op��o inexistente...");
							break;
						}
					}while(criar);
				break;
				case 2://Selecionar Conta
					System.out.println("Insira o numero da conta a ser selecionada: ");
					contaselect = teclado.nextInt();
					itau.pesquisarCliente(contaselect);
					do {
						switch( SubMenu() ) {
						case 1://depositar
							System.out.println("Insira o valor a Depositar em conta.\n");
							itau.pesquisarCliente(contaselect).Depositar(teclado.nextDouble());
							System.out.println("-Opera��o Concluida-\n");
							break;
						case 2: //sacar	
							System.out.println("Insira o valor a Sacar da conta.\n");
							itau.pesquisarCliente(contaselect).Sacar(teclado.nextDouble());
							System.out.println("-Opera��o Concluida-\n");
							break;
						case 3://transferir
							System.out.println("Digite o numero da conta da transferencia.\n");
							transferencia = teclado.nextInt();					
							System.out.println("Digite o valor a ser transferido.\n");
							itau.pesquisarCliente(contaselect).Transferir(teclado.nextDouble(), itau.pesquisarCliente(transferencia));
							System.out.println("-Opera��o Concluida-\n");
							break;
						case 4://gerarRelatorio
							itau.mostrarDados();
							System.out.println("-Opera��o Concluida-\n");
							break;
						case 5://backtoMenu
							System.out.println(":) At� mais!\n\n\n");
							select = false;
							break;
						default:
							System.out.println("op��o inexistente...");
						break;
						}
					}while(select);
			 
				break;
				case 3://Remover Conta
					System.out.println("Insira o numero da conta � remover: ");
					itau.removeCliente(teclado.nextInt());
				 
				break;
				case 4://Gerar Relatorio
					itau.mostrarDados();
				 
				break;
				case 5://Finalizar
					System.out.println(":) At� mais!\n\n\n");
					opcao = false; 
					
				break;
				default:
					System.out.println("Voc� selecionou uma op��o que n�o existe, por favor tente novamente!\n");

				break;
			}
		}while(opcao);

	}
	public static int Menu() {
		System.out.println("********Menu Inicial********\n\n");
		System.out.println("1- Criar Conta\n");
		System.out.println("2- Selecionar Conta\n");
		System.out.println("3- Remover Conta\n");
		System.out.println("4- Gerar Relatorio\n");
		System.out.println("5- Finalizar\n");
		System.out.println("****************************\n");
		System.out.println("Digite a op��o desejada:");
		return teclado.nextInt();
		
	}
	public static int SubMenu() {
		System.out.println("*****Opera��es em Conta*****\n\n");
		System.out.println("1- Depositar\n");
		System.out.println("2- Sacar\n");
		System.out.println("3- Transferir\n");
		System.out.println("4- Gerar Relatorio\n");
		System.out.println("5- Retornar ao menu anterior\n");
		System.out.println("****************************\n");
		System.out.println("Digite a op��o desejada:");
		return teclado.nextInt();
	}
	public static int CriandoConta() {
		System.out.println("*****Criando Contas*****\n\n\n");
		System.out.println("1- Conta Corrente\n");
		System.out.println("2- Conta Poupanca\n");
		System.out.println("Selecione o tipo de Conta:");
		return teclado.nextInt();
	}

	public static ContaBancaria ContaCorrente() {
		System.out.println("Insira o numero da Conta: ");
		int numero = teclado.nextInt();
		System.out.println("Insira o saldo da Conta: ");
		double saldo = teclado.nextDouble();
		ContaBancaria conta1 = new ContaCorrente(numero, saldo, 50);
		return conta1;
	}
	
	public static ContaBancaria ContaPoupanca() {
		System.out.println("Insira o numero da Conta: ");
		int numero = teclado.nextInt();
		System.out.println("Insira o saldo da Conta: ");
		double saldo = teclado.nextDouble();
		ContaBancaria conta1 = new ContaPoupanca(numero, saldo, 1000);
		return conta1;
	}
	
}
