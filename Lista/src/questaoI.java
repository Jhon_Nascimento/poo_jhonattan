import java.util.Scanner;

public class questaoI {

	public static Scanner calc = new Scanner (System.in);
	public static void main(String[] args) {

		System.out.println("Insira o primeiro numero: ");
		float n1 = calc.nextFloat();
		System.out.println("Insira o segundo numero: ");
		float n2 = calc.nextFloat();
		System.out.println("Observe o menu abaixo e escolha a opera��o desejada:\n"
				+ "1- Adi��o\n" + "2- Subtra��o\n" + "3- Multiplica��o\n" + "4- Divis�o\n" + "Insira sua opera��o desejada:");
		int operacion = calc.nextInt();
		operacao(operacion, n1, n2);
	}
	public static void operacao(int op, float n1, float n2) {
		float result = 0;
		switch(op) {
			case 1:
				result = n1 + n2;
				System.out.println("Resultado: "+result);
				break;
			case 2:
				result = n1 - n2;
				System.out.println("Resultado: "+result);
				break;
			case 3:
				result = n1 * n2;
				System.out.println("Resultado: "+result);
				break;
			case 4:
				if(n2 != 0) {
					result = n1 / n2;
					System.out.println("Resultado: "+result);
					break;
				}else {
					System.out.println("Divisor igual a zero, tente outro numero...");
					break;
				}
			default:
				System.out.println("Opera��o Inv�lida");
		}
	}

}
